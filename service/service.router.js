const express = require('express');
const allowCrossDomain = require('cors');

module.exports = () => {
  const routes = express.Router();
  routes.use(allowCrossDomain({
    methods: ['GET', 'POST', 'HEAD', 'OPTIONS']
  }));

  routes.get('/', require('./home.controller'));

  return routes;
};
