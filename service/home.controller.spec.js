let request = require('supertest');
let app = require('../bin/app');

describe('Home controller', () => {
  it('Should be ok to get /', (done) => {
    request(app)
      .get('/api/v1/simple-service')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect('Content-Length', '25')
      .expect(200, 'Service is up and running')
      .end((err, res) => {
        if (err) throw err;
        done();
      });
  });
});
