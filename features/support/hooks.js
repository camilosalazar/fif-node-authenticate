let beforeCallCount = 0;
let beforeFeatureCallCount = 0;
const winston = require('winston');

module.exports = function() {
  this.Before(function(scenario) {
    winston.info('---> Before hook called', ++beforeCallCount, 'time' + (beforeCallCount !== 1 ? 's' : ''));
  });

  this.Before('@sections', function(scenario) {
    winston.info('---> A test tagged with @sections is about to be run');
  });

  this.Before('@badges', function(scenario) {
    winston.info('---> A test tagged with @badges is about to be run');
  });

  this.registerHandler('BeforeFeature', function(ev, callback) {
    // Be careful as this.World is not set up yet!
    winston.info('---> this.World?', typeof this.World);

    winston.info('---> BeforeFeature hook called', ++beforeFeatureCallCount, 'time' + (beforeFeatureCallCount !== 1 ? 's' : ''));

    // Don't forget to make a callback!
    callback();
  });
};
