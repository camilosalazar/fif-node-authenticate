/**
 * Export a World constructor which will be the scope of all step definitions.
 */
const zombie = require('zombie');
const winston = require('winston');
let worldCallCount = 0;

function World() {
  winston.info('---> World initalised', ++worldCallCount, 'time' + (worldCallCount !== 1 ? 's' : ''));
  this.browser = new zombie({
    runScripts: false
  });
}

module.exports = function() {
  this.World = World;
};
