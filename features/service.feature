Feature: Check service is up and running

  Background:
    Given I have app up and running

  Scenario Outline: Display correctly service is running
    When I go to "<serviceUrl>"
    Then I can see title "<serviceTitle>"

    Examples:
      | serviceUrl | serviceTitle |
      | http://localhost:3006/api/v1/simple-service | Service is up and running |
