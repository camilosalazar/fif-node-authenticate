const MongoInMemory = require('mongo-in-memory');
const winston = require('winston');

var mongoServerInstance = new MongoInMemory(27018);

mongoServerInstance.start((error, config) => {
  if (error) {
    winston.error(error);
  } else {
    winston.info("In memory mongodb conf:");
    winston.info("host: " + config.host);
    winston.info("port: " + config.port);
  }
});
