/**
 * error handler middleware.
 */

'use strict';

const winston = require('winston');

const handler = (err, request, response, next) => {
  if (err && err.stack) {
    winston.log('error', 'Generic error found ', err.stack);
  }
  response.status(500);
  response.send('Something broke!');
};

module.exports = handler;
