// Bring Mongoose into the app
const mongoose = require('mongoose');
const config = require('../config/index');
const winston = require('winston');
const util = require('util');

const init_db = () => {
  winston.log('info', 'Inicializando mongodb');

  const mongo_url = util.format('mongodb://%s:%s@%s:%s/%s?ssl=true',
    process.env.MONGO_USER, process.env.MONGO_PASS,
    process.env.MONGO_HOST, process.env.MONGO_PORT,
    config.mongo_database);

  winston.log('info', 'MONGO_USER=' + process.env.MONGO_USER);
  winston.log('info', 'MONGO_HOST=' + process.env.MONGO_HOST);
  winston.log('info', 'MONGO_PORT=' + process.env.MONGO_PORT);

  // Create the database connection
  mongoose.Promise = global.Promise;
  mongoose.connect(mongo_url);

  // CONNECTION EVENTS
  // When successfully connected
  mongoose.connection.on('connected', function() {
    winston.log('info', 'Mongoose default connection open to',
      config.mongo_database);
  });

  // If the connection throws an error
  mongoose.connection.on('error', function(err) {
    winston.log('error', 'Mongoose default connection', err);
  });

  // When the connection is disconnected
  mongoose.connection.on('disconnected', function() {
    winston.log('info', 'Mongoose default connection disconnected');
  });

  // If the Node process ends, close the Mongoose connection
  process.on('SIGINT', function() {
    mongoose.connection.close(function() {
      winston.log('info', 'Mongoose default connection disconnected  through app termination');
      process.exit(0);
    });
  });

};

module.exports = init_db;
