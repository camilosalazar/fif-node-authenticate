/*
 * Punto central para registro de uso de tecnologias y exposicion de
 * endpoints.
 */

'use strict';

const winston = require('winston');
winston.level = process.env.LOG_LEVEL || 'silly';

winston.log('info', 'Configuracion:');
winston.log('info', 'export NODE_ENV=[local, development, qa, staging, production] => define el ambiente');
winston.log('info', 'export PORT=numero => define el puerto a escuchar peticiones');
winston.log('info', 'export MONGO_USER=the_user => define el usuario para mongodb');
winston.log('info', 'export MONGO_PASS=the_pass => define la contraseña para mongodb');
winston.log('info', 'export MONGO_HOST=the_host => define el host para mongodb');
winston.log('info', 'export MONGO_PORT=the_port => define el puerto para mongodb');
winston.log('info', 'export LOG_FOLDER=log/dir/path => define ruta para guardar logs (debe existir)');
winston.log('info', 'export LOG_FORMAT=[combined, common, dev, format] => define formato de log');
winston.log('info', 'export LOG_LEVEL=[error, warn, info, verbose, debug, silly] => define nivel de log');
winston.log('info', 'LOG_LEVEL=' + winston.level);

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// specify a single subnet
app.set('trust proxy', 'loopback');

const configuredMorgan = require('./morgan.config');
app.use(configuredMorgan());

const serviceRouter = require('../service/service.router');
app.use('/api/v1/simple-service', serviceRouter());

const errorHandler = require('./error.handler');
app.use(errorHandler);

module.exports = app;
