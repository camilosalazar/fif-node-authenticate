const winston = require('winston');
const fs = require('fs');
const path = require('path');
const FileStreamRotator = require('file-stream-rotator');

module.exports = () => {
  const morgan = require('morgan');
  morgan.token('type', function(req, res) {
    return req.headers['content-type'];
  });

  const logFormat = process.env.LOG_FORMAT || 'dev';
  winston.log('info', 'LOG_FORMAT=' + logFormat);

  return morgan(logFormat, getLogOptions());
};


const getLogOptions = () => {
  let logOptions = {};

  const logFolder = process.env.LOG_FOLDER || __dirname + '/log';
  winston.log('info', 'LOG_FOLDER=' + logFolder);

  if (fs.existsSync(logFolder)) {
    const logStream = FileStreamRotator.getStream({
      date_format: 'YYYYMMDD',
      filename: path.join(logFolder, 'access-%DATE%.log'),
      frequency: 'daily',
      verbose: false
    });

    logOptions = {
      stream: logStream
    };
  }
  return logOptions;
};
