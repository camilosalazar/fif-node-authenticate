'use strict';

const util = require('util');
const winston = require('winston');
const _ = require('lodash');

const env = process.env.NODE_ENV || 'local';
winston.log('info', 'NODE_ENV=' + env);
const configFileName = util.format('./%s.config.js', env);
const config = require(configFileName);

const commonConfig = require('./common.config');
_.merge(config, commonConfig);

module.exports = config;
