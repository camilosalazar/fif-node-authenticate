const config = require('./index');
const expect = require('chai').expect;

describe('Config:', () => {
  it('should read common config', () => {
    const expected = 'http://mdwcorp.falabella.com/common/schema/clientservice';
    expect(config.client_service_namespace).to.equal(expected);
  });

  it('should read config (local)', () => {
    expect(config.secret).to.equal('secret-key-local');
  });

});
